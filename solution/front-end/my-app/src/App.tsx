import React, { useState, useEffect } from "react";
import ScoreBoard from "./Components/ScoreBoard";
import GameBoard from "./Components/GameBoard";
import Opponent from "./Components/OpponentMove";
import Footer from "./Components/Footer";
import GameService from "./Service/GameService";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  const [score, setScore] = useState(0);
  const [maxScore, setMaxScore] = useState(0);

  useEffect(() => {
    async function updateMaxScore() {
      var data = await GameService.getLongestWinStreak();
      if (data) {
        setMaxScore(data.data);
      }
    }
    if (maxScore === 0) {
      updateMaxScore();
    }
  }, [maxScore]);

  return (
    <Router>
      <Route path="\/*">
        <ScoreBoard
          score={score}
          maxScore={maxScore}
          updateScore={(e: any) => setScore(e)}
        />
      </Route>
      <Switch>
        <Route path="/opponent">
          <Opponent
            score={score}
            updateScore={(e: any) => setScore(e)}
            maxScore={maxScore}
            updateMaxScore={(e: any) => setMaxScore(e)}
          />
        </Route>
        <Route path="/">
          <GameBoard />
        </Route>
      </Switch>
      <Route path="\/*">
        <Footer />
      </Route>
    </Router>
  );
}

export default App;
