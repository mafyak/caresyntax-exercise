import OpenAPI from "./OpenAPI";

const GAME_API_URL = "/api/game";

class GameService {
  getLongestWinStreak() {
    return OpenAPI.get(`${GAME_API_URL}/longestWinStreak`);
  }

  setLongestWinStreak(value) {
    const achievement = {};
    achievement.key = "longestWinStreak";
    achievement.value = value;
    return OpenAPI.post(`${GAME_API_URL}/setAchievement`, achievement);
  }

  play() {
    return OpenAPI.post(`${GAME_API_URL}/play`);
  }
}

export default new GameService();
