import axios from "axios";

const OpenAPI = axios.create({
  baseURL: "http://localhost:8080",
});

export default OpenAPI;
