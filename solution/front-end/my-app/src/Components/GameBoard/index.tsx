import React from "react";
import "./index.css";
import paper from "./icon-paper.svg";
import rock from "./icon-rock.svg";
import scissors from "./icon-scissors.svg";
import Button from "../Button";
import { useHistory } from "react-router-dom";

const GameBoard = () => {
  let history = useHistory();

  function userChoice(value: string) {
    localStorage.setItem("userChoice", value);
    history.push({ pathname: "/opponent" });
  }

  return (
    <div className="field">
      <div className="buttons">
        <div className="game-board-container gb-top">
          <div className="paper circle" onClick={() => userChoice("paper")}>
            <Button
              image={paper}
              color="hsl(230, 89%, 65%)"
              shadowColor="0 10px 0 -1px hsl(230, 89%, 62%)"
              sizeIncrement={1}
            />
          </div>
          <div
            className="scissors circle"
            onClick={() => userChoice("scissors")}
          >
            <Button
              image={scissors}
              color="hsl(40, 84%, 53%)"
              shadowColor="0 10px 0 -1px hsl(39, 89%, 49%)"
              sizeIncrement={1}
            />
          </div>
        </div>
        <div className="game-board-container gb-bottom">
          <div className="rock circle" onClick={() => userChoice("rock")}>
            <Button
              image={rock}
              color="hsl(349, 70%, 56%)"
              shadowColor="0 10px 0 -1px hsl(349, 71%, 52%)"
              sizeIncrement={1}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default GameBoard;
