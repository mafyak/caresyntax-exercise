import React from "react";
import "./index.css";
import rulesImg from "./image-rules.svg";
import closeImg from "./icon-close.svg";

function Popup(props: any) {
  return props.trigger ? (
    <div className="popup">
      <div className="popup-inner">
        <div className="popup-row">
          rules
          <button className="btn-popup" onClick={() => props.setTrigger(false)}>
            <img src={closeImg} alt="" />
          </button>
        </div>
        <div className="popup-row">
          <img src={rulesImg} alt="Our rules" />
        </div>
      </div>
    </div>
  ) : (
    <></>
  );
}

export default Popup;
