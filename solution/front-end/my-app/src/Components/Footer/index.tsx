import React, { useState } from "react";
import "./index.css";
import Popup from "../Popup";

const Footer = () => {
  const [isPopUpActive, setIsPopUpActive] = useState(false);

  return (
    <>
      <Popup trigger={isPopUpActive} setTrigger={setIsPopUpActive}>
        Popup
      </Popup>
      <div className="rules-button-container">
        <div
          className="rules-button-block"
          onClick={() => setIsPopUpActive(true)}
        >
          rules
        </div>
      </div>
    </>
  );
};

export default Footer;
