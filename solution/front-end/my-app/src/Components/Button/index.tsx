import React from "react";
import "./index.css";

interface Props {
  image: string;
  color: string;
  shadowColor: string;
  sizeIncrement: number;
}

const Button = ({ image, color, shadowColor, sizeIncrement }: Props) => {
  // @todo spike: should I use windowDimensions to track window size?

  // const [windowDimensions, setWindowDimensions] = useState(
  //   getWindowDimensions()
  // );

  // useEffect(() => {
  //   window.addEventListener("resize", updateWindowDimensions);

  //   function updateWindowDimensions() {
  //     setWindowDimensions(getWindowDimensions());
  //   }
  // }, []);

  // function getWindowDimensions() {
  //   const { innerWidth: width, innerHeight: height } = window;
  //   return {
  //     width,
  //     height,
  //   };
  // }

  return (
    <div
      style={{
        backgroundColor: color,
        boxShadow: shadowColor,
        // height: (windowDimensions.width / 9) * sizeIncrement + "px",
        // width: (windowDimensions.width / 9) * sizeIncrement + "px",
        height: 10 * sizeIncrement + "vw",
        width: 10 * sizeIncrement + "vw",
      }}
      className="button-outer"
    >
      <div className="button-inner">
        <img src={image} className="center-image" alt="button" width="50%" />
      </div>
    </div>
  );
};

export default Button;
