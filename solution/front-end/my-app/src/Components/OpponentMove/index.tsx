import React, { useState, useEffect } from "react";
import "./index.css";
import PrintButton from "./PrintButton";
import GameService from "../../Service/GameService";
import { useHistory } from "react-router-dom";

interface Props {
  updateScore: Function;
  score: number;
  updateMaxScore: Function;
  maxScore: number;
}

const OpponentMove = ({
  updateScore,
  score,
  updateMaxScore,
  maxScore,
}: Props) => {
  const [userMove, setUserMove] = useState("");
  const [houseMove, setHouseMove] = useState("");
  const [isEnd, setIsEnd] = useState(false);
  const [isDraw, setIsDraw] = useState(false);
  const [userWon, setUserWon] = useState(false);
  let history = useHistory();

  useEffect(() => {
    var userChoice = localStorage.getItem("userChoice");
    if (userChoice) {
      setUserMove(userChoice);
    }
    async function makeHouseMove() {
      var response = await GameService.play();
      if (response) {
        var houseChoice = response.data.toString().toLowerCase();
        setTimeout(() => setHouseMove(houseChoice), 1000);
        setTimeout(
          () =>
            decideTheWinner(
              userChoice ? userChoice.toString() : "",
              houseChoice
            ),
          1500
        );
      }
    }

    if (!houseMove) {
      makeHouseMove();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function decideTheWinner(userChoice: string, houseChoice: string) {
    if (userChoice === houseChoice) {
      setIsDraw(true);
    } else if (
      (userChoice === "scissors" && houseChoice === "paper") ||
      (userChoice === "paper" && houseChoice === "rock") ||
      (userChoice === "rock" && houseChoice === "scissors")
    ) {
      setUserWon(true);
      incrementScore();
      if (score + 1 > maxScore) {
        updateMaxScore(score + 1);
        GameService.setLongestWinStreak(score + 1);
      }
    } else {
      setUserWon(false);
      resetScore();
    }
    setIsEnd(true);
  }

  function incrementScore() {
    updateScore(score + 1);
  }

  function resetScore() {
    updateScore(0);
  }

  function playAgain() {
    history.push({ pathname: "/" });
  }

  return (
    <div className="container">
      <div
        className="picks wrap"
        style={{ flexDirection: "row", justifyContent: "space-between" }}
      >
        <div className={isEnd ? "col-lg-2 " : "col-lg-3 "}></div>
        <div className={isEnd ? "col-lg-3 " : "col-lg-3 house-pick"}>
          <div className="header">You picked</div>
          <div className="button">
            <PrintButton
              button={userMove}
              winShadow={isEnd && !isDraw && userWon}
            />
          </div>
        </div>
        <div className={isEnd ? "col-lg-3 " : "hide"}>
          <div className="result">
            <div className="result-content">
              {isDraw ? "DRAW" : userWon ? "YOU WON" : "YOU LOST"}
            </div>
            <div className="result-content-button">
              <button className="btn align-middle" onClick={() => playAgain()}>
                PLAY AGAIN
              </button>
            </div>
          </div>
        </div>
        <div className={isEnd ? "col-lg-3 " : "col-lg-3 house-pick"}>
          <div className="header">House picked</div>
          <div className="button">
            <PrintButton
              button={houseMove}
              winShadow={isEnd && !isDraw && !userWon}
            />
          </div>
        </div>
        <div className={isEnd ? "col-lg-2 " : "col-lg-3 "}></div>
      </div>
    </div>
  );
};

export default OpponentMove;
