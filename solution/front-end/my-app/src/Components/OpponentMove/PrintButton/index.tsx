import React from "react";
import Button from "../../Button";
import paper from "../../GameBoard/icon-paper.svg";
import rock from "../../GameBoard/icon-rock.svg";
import scissors from "../../GameBoard/icon-scissors.svg";
import "./index.css";

interface Props {
  button: string;
  winShadow: boolean;
}

const PrintButton = ({ button, winShadow }: Props) => {
  const winShadowCss =
    ", 0px 0px 0px 4vw rgb(255 255 255 / 10%), 0px 0px 0px 8vw rgb(255 255 255 / 10%), 0px 0px 0px 12vw rgb(255 255 255 / 10%)";
  const paperShadow = "0 10px 0 -1px hsl(230, 89%, 62%)";
  const rockShadow = "0 10px 0 -1px hsl(349, 71%, 52%)";
  const scissorsShadow = "0 10px 0 -1px hsl(39, 89%, 49%)";

  if (button === "paper") {
    return (
      <Button
        image={paper}
        color="hsl(230, 89%, 65%)"
        shadowColor={winShadow ? paperShadow + winShadowCss : paperShadow}
        sizeIncrement={1.8}
      />
    );
  } else if (button === "rock") {
    return (
      <Button
        image={rock}
        color="hsl(349, 70%, 56%)"
        shadowColor={winShadow ? rockShadow + winShadowCss : rockShadow}
        sizeIncrement={1.8}
      />
    );
  } else if (button === "scissors") {
    return (
      <Button
        image={scissors}
        color="hsl(40, 84%, 53%)"
        shadowColor={winShadow ? scissorsShadow + winShadowCss : scissorsShadow}
        sizeIncrement={1.8}
      />
    );
  } else {
    return <div className="empty-spot"></div>;
  }
};

export default PrintButton;
