import React from "react";
import "./index.css";
import logo from "./logo.svg";

interface Props {
  score: number;
  maxScore: number;
  updateScore: Function;
}

function ScoreBoard({ score, maxScore, updateScore }: Props) {
  return (
    <div className="score-board">
      <div className="float-left sticky-center">
        <img src={logo} alt="logo" />
      </div>
      <div className="float-right sticky-center">
        <div className="score-square score-square-max">
          <div className="score-text">max score</div>
          <div className="score-number">{maxScore}</div>
        </div>
      </div>
      <div className="float-right sticky-center">
        <div className="score-square">
          <div className="score-text">score</div>
          <div className="score-number">{score}</div>
        </div>
      </div>
    </div>
  );
}

export default ScoreBoard;
