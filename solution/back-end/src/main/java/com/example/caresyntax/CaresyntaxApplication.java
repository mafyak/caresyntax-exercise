package com.example.caresyntax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class CaresyntaxApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaresyntaxApplication.class, args);
	}

}
