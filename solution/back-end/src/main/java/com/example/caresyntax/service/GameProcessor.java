package com.example.caresyntax.service;

import com.example.caresyntax.entity.Achievement;
import com.example.caresyntax.entity.ChoiceType;

public interface GameProcessor {

    ChoiceType getHouseChoice();
    Integer getLongestWinStreak();
    void setAchievement(Achievement value);
    
}
