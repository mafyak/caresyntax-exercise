package com.example.caresyntax.dao;

import com.example.caresyntax.entity.Achievement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AchievementsRepository extends CrudRepository<Achievement, String> {
}
