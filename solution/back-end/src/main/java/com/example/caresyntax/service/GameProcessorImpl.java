package com.example.caresyntax.service;

import com.example.caresyntax.dao.AchievementsRepository;
import com.example.caresyntax.entity.Achievement;
import com.example.caresyntax.entity.ChoiceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class GameProcessorImpl implements GameProcessor {

    private final Random random;
    private final AchievementsRepository achievementsRepository;

    @Autowired
    public GameProcessorImpl(AchievementsRepository achievementsRepository) {
        this.random = new Random();
        this.achievementsRepository = achievementsRepository;
    }

    @Override
    public ChoiceType getHouseChoice() {
        return ChoiceType.values()[random.nextInt(3)];
    }

    @Override
    public Integer getLongestWinStreak() {
        return Integer.parseInt(achievementsRepository.findById("longestWinStreak").orElse(new Achievement("", "0")).getValue());
    }

    @Override
    public void setAchievement(Achievement longestWinStreak) {
        achievementsRepository.save(longestWinStreak);
    }
}
