package com.example.caresyntax.entity;

public enum ChoiceType {

    PAPER, SCISSORS, ROCK
}
