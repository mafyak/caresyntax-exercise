package com.example.caresyntax.controller;

import com.example.caresyntax.entity.Achievement;
import com.example.caresyntax.service.GameProcessor;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/api/game")
@AllArgsConstructor
public class GameAPI {

    private final GameProcessor gameProcessor;

    @GetMapping("/longestWinStreak")
    @ApiOperation(value = "Get longest win streak from achievements")
    public Integer getLongestWinStreak() {
        return gameProcessor.getLongestWinStreak();
    }

    @PostMapping("/setAchievement")
    @ApiOperation(value = "Set new longest win streak")
    public void setAchievement(@RequestBody Achievement achievement) {
        gameProcessor.setAchievement(achievement);
    }

    @PostMapping("/play")
    @ApiOperation(value = "Provides a random house move")
    public String play() {
        return gameProcessor.getHouseChoice().name();
    }
}
